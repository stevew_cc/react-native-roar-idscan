//
//  DriverLicense.h
//  CodeLib
//
//  Created by Дмитрий Грищенко on 22/02/2019.
//  Copyright © 2019 Abycus. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>
#import <CoreGraphics/CoreGraphics.h>
#import <CoreVideo/CoreVideo.h>
#import <CoreMedia/CoreMedia.h>
#if TARGET_IPHONE_SIMULATOR
#import <BarcodeScannerSimulator/BarcodeScannerSimulator.h>
#else
#import <BarcodeScanner/BarcodeScanner.h>
#endif

typedef enum eMainScreenState {
    NORMAL,
    LAUNCHING_CAMERA,
    CAMERA,
    CAMERA_DECODING,
    DECODE_DISPLAY,
    CANCELLING
} MainScreenState;

#define DecoderResultNotification @"DecoderResultNotification"

@interface DecoderResult : NSObject {
    BOOL succeeded;
    NSString *result;
}

@property (nonatomic, assign) BOOL succeeded;
@property (nonatomic, retain) NSString *result;

+(DecoderResult *)createSuccess:(NSString *)result;
+(DecoderResult *)createFailure;

@end

@interface ScannerViewController : UIViewController <AVCaptureVideoDataOutputSampleBufferDelegate, UIAlertViewDelegate>

@property (nonatomic, assign) MainScreenState state;
@property (weak, nonatomic) IBOutlet UIButton *btn;
@property (strong, nonatomic) IBOutlet UILabel *detectLbl;
@property (strong, nonatomic) IBOutlet UIStackView *timerSV;
@property (strong, nonatomic) IBOutlet UIButton *parseMRZBtn;
@property (strong, nonatomic) IBOutlet UIView *scanRect;

@property (nonatomic, retain) AVCaptureSession *captureSession;
@property (nonatomic, retain) AVCaptureVideoPreviewLayer *prevLayer;
@property (nonatomic, retain) Barcode2DScanner* scanner;
@property (nonatomic, retain) AVCaptureDevice *device;
@property (nonatomic, retain) NSTimer *focusTimer;
- (IBAction)closeBtn:(id)sender;
- (IBAction)parseMRZ:(id)sender;
- (void)decodeResultNotification: (NSNotification *)notification;
- (void)initCapture;
- (void) startScanning;
- (void) stopScanning;
- (void) toggleTorch;
@end


