//
//  MRZ_C_Parser.h
//  MRZ_C_Parser
//
//  Created by Alex S. on 21/02/2019.
//  Copyright © 2019 Abycus. All rights reserved.
//

#import <UIKit/UIKit.h>

//! Project version number for MRZ_C_Parser.
FOUNDATION_EXPORT double MRZ_C_ParserVersionNumber;

//! Project version string for MRZ_C_Parser.
FOUNDATION_EXPORT const unsigned char MRZ_C_ParserVersionString[];

@interface NSString (MRZParser)
- (NSDictionary *)parseMRZStringForDict;
@end

// In this header, you should import all the public headers of your framework using statements like #import <MRZ_C_Parser/PublicHeader.h>
@interface MRZParser : NSObject

- (NSDictionary*) parseMRZStringForDictionary:(NSString*)inputString;


@end



