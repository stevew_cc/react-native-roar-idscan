//
//  AppDelegate.h
//  DLPCameraTest
//
//  Created by Ossir on 13/06/2019.
//  Copyright © 2019 abycus. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

